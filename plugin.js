export async function init(pluginAPI) {
  await pluginAPI.loadStylesheets(["./css/oaam-hardware-editor.css"]);
  pluginAPI.implement("xgee.models", {
    modelPath: pluginAPI.getPath() + "Hardware.editorModel",
  });

  return true;
}

export var meta = {
  id: "editor.oaam.hardware",
  description: "Editor for the Harwdare Layer of OAAM",
  author: "Matthias Brunner",
  version: "0.1.0",
  requires: ["ecore", "editor"],
};
